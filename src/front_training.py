from tkinter import *
import back_unit, back_game, back_city, outils

class Interface_Entrainement(Toplevel):
    def __init__(self,master, game):
        Toplevel.__init__(self, master)
        self.master = master
        self.game = game
        self.city = game.ville_selectionnee

        self.unit_selectionnee = None
        self.all_unites = outils.charger_fichier_json("UNITS")
        print(self.all_unites)


        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()


    def charger_images(self):
        self.images = {}
        self.images[None] = PhotoImage(file="../pics/window/ICON_pas_travail.png")
        self.images["BTN_OK"] = PhotoImage(file="../pics/window/ICON_confirmer.png")
        for unit in self.all_unites:
            self.images[unit] = PhotoImage(file="../pics/units/"+self.all_unites[unit]["icon"])

    def creer_widgets(self):
        self.lbl_cityname = Label(master=self, text=self.game.ville_selectionnee.name)
        self.btn_quitter = Button(master=self, image=self.images["BTN_OK"], command=self.acheter)  # text="Quitter",
        self.zone_1 = Frame(master=self)

        self.lbl_en_tranning = Label(master=self,
                                         text="En Traning :\t",
                                         image=self.images[None],
                                         compound=BOTTOM)
        self.lbl_en_traning_info = Label(master=self,
                                              text="Aucun unite sélectionné")

        self.btn_traning = []
        for unit in self.all_unites:
            self.btn_traning.append(Button(master=self.zone_1,
                                                 image=self.images[unit] ,
                                                 command=lambda x=unit:self.on_btn_unit_click(x)))
    def configurer_widgets(self):
        self.lbl_cityname.config(font="Arial 30 bold")
        self.lbl_en_tranning.config(font="Arial 12 italic")
        self.lbl_en_traning_info.config(font="Arial 14", justify=LEFT)
        if self.unit_selectionnee is None:
            self.lbl_en_traning_info.config(fg="red")
        else:
            self.lbl_en_traning_info.config(fg="black")
    def placer_widgets(self):
        self.lbl_cityname.grid(row=0, column=0, columnspan=3, rowspan=1)
        self.zone_1.grid(row=1, column=0, rowspan=3, sticky=N)
        for i, btn in enumerate(self.btn_traning):
            btn.grid(column=i % 3, row=i // 3)

        self.lbl_en_tranning.grid(row=1, column=1, sticky=N)
        self.lbl_en_traning_info.grid(row=2, column=1, sticky=N)
        self.btn_quitter.grid(row=3, column=1, sticky=S)

    def actualiser(self):
        if self.unit_selectionnee is None:
            self.lbl_en_tranning.config(image=self.images[None])
            self.lbl_en_traning_info.config(text="Aucun bâtiment sélectionné",
                                                 fg="red")
        else:
            self.lbl_en_tranning.config(image=self.images[self.unit_selectionnee])
            self.lbl_en_traning_info.config(text=str(back_unit.Unit(self.unit_selectionnee)),
                                                 fg="black")
    def on_btn_unit_click(self,unit):
        """Met le batiment selectionné en construction"""
        self.unit_selectionnee = unit
        self.actualiser()

    def acheter(self):
        print("essayer")
        if self.unit_selectionnee is not None:
            print("achete ", self.unit_selectionnee)
            #if self.all_unites[self.unit_selectionnee]["cost-gold"] == self.game.ville_selectionnee.resources["gold"] and self.all_unites[self.unit_selectionnee]["cost-population"] == self.game.ville_selectionnee.population and self.all_unites[self.unit_selectionnee]["cost-food"] == self.game.ville_selectionnee.resources["food"]:
            self.game.ville_selectionnee.peut_ajouter_unite(self.unit_selectionnee)
            self.destroy()



if __name__ == '__main__':
    j = back_game.Jeu("../maps/MAP_WORLD.json","NATION_USA")
    j.selectionner_ville("CITY_Washington_D_C")

    fenetre = Tk()
    btn = Button(fenetre,
                 text="Construire",
                 command=lambda: Interface_Entrainement(fenetre,j))
    btn.grid()
    fenetre.mainloop()