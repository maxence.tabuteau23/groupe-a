
class Interface_Tree_Plane(Frame):
    def __init__(self, master):
        """
        Constructeur initial de l'interface.
        """
        Frame.__init__(self, master)
        self.master = master
        self.pays = None
        self.charger_images()

        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()

    def creer_widgets(self):
        """
        Crée les différents boutons et labels de l'interface de base.
        """
        self.lbl_tier1 = Label(master=self)
        self.lbl_tier2 = Label(master=self)
        self.lbl_tier3 = Label(master=self)
        self.lbl_tier4 = Label(master=self)
        self.lbl_tier5 = Label(master=self)
        self.lbl_tier6 = Label(master=self)

        self.lbl_classeB = Label(master=self)
        self.lbl_classeC = Label(master=self)
        self.lbl_classeT = Label(master=self)


        if self.pays !=None:

            self.img_C1 = Label(master=self)
            self.img_C2 = Label(master=self)
            self.img_C3 = Label(master=self)
            self.img_C4 = Label(master=self)
            self.img_C5 = Label(master=self)
            self.img_C6 = Label(master=self)

            self.img_B2 = Label(master=self)
            self.img_B3 = Label(master=self)
            self.img_B5 = Label(master=self)

            self.img_T5 = Label(master=self)

            if self.pays == "France":
                self.img_B4 = Label(master=self)

                self.img_T2 = Label(master=self)
                self.img_T3 = Label(master=self)

            if self.pays == "URSS":
                self.img_B6 = Label(master=self)

                self.img_T3 = Label(master=self)
                self.img_T6 = Label(master=self)

            if self.pays == "USA":
                self.img_B4 = Label(master=self)
                self.img_B6 = Label(master=self)

                self.img_T2 = Label(master=self)
                self.img_T4 = Label(master=self)






    def configurer_widgets(self):
        """
        Configure les widgets déjà créés précédemment.
        """
        France={}
        URSS={}
        USA={}

        self.lbl_tier1.config(text="I")
        self.lbl_tier2.config(text="II")
        self.lbl_tier3.config(text="III")
        self.lbl_tier4.config(text="IV")
        self.lbl_tier5.config(text="V")
        self.lbl_tier6.config(text="VI")


        self.lbl_classeB.config(text="BOMBARDIER")
        self.lbl_classeC.config(text="CHASSEUR")
        self.lbl_classeT.config(text="TRANSPORT")








    def charger_images(self):
        """
        Charge les différentes images nécessaires à l'interface.
        """
        data = charger_fichier_json("UNITS")
        self.images = {}
        tab = ["air_off", "air_on", "sol_on", "sol_off", "boost_on", "boost_off", "boost", "attaque", "repos",
               "mouvement", "rien"]
        for elt in tab:
            self.images[str(elt)] = PhotoImage(file="../pics/window/ICON_" + str(elt) + ".png")
        for unit in data:
            self.images[str(unit)] = PhotoImage(file="../pics/units/" + data[unit]["icon"])

    def placer_widgets(self):
        """
        Place les différents boutons et labels à l'initialisation.
        """
        self.img_unit.grid(column=0, row=0, columnspan=1, rowspan=7)

        self.btn_attaque.grid(column=3, row=7, columnspan=1, rowspan=2)
        self.btn_repos.grid(column=3, row=9, columnspan=1, rowspan=2)
        self.btn_deplacer.grid(column=4, row=7, columnspan=1, rowspan=2)

        self.lbl_pv.grid(column=2, row=1, columnspan=1, rowspan=1)
        self.lbl_degats.grid(column=2, row=3, columnspan=1, rowspan=1)
        self.lbl_energie.grid(column=2, row=5, columnspan=1, rowspan=1)
        self.lbl_niveau.grid(column=4, row=9, columnspan=1, rowspan=2)

        self.prb_pv.grid(column=1, row=0, columnspan=4, rowspan=1)
        self.prb_degats.grid(column=1, row=2, columnspan=4, rowspan=1)
        self.prb_energie.grid(column=1, row=4, columnspan=4, rowspan=1)

        self.ind_sol.grid(column=1, row=6, columnspan=1, rowspan=1)
        self.ind_air.grid(column=3, row=6, columnspan=2, rowspan=1)
        self.ind_pv.grid(column=0, row=7, columnspan=1, rowspan=1)
        self.ind_energie.grid(column=0, row=8, columnspan=1, rowspan=1)
        self.ind_degat.grid(column=0, row=9, columnspan=1, rowspan=1)
        self.ind_vitesse.grid(column=0, row=10, columnspan=1, rowspan=1)

        self.lbl_name.grid(column=1, row=7, columnspan=2, rowspan=1)
        self.lbl_taille.grid(column=1, row=8, columnspan=2, rowspan=1)
        self.lbl_type.grid(column=1, row=9, columnspan=2, rowspan=1)
        self.lbl_statut.grid(column=1, row=10, columnspan=2, rowspan=1)

    def actualiser(self, unit=None):
        """
        Met à jour l'interface avec l'unité sélectionnée.
        """
        self.unite_selectionnee = unit
        self.configurer_widgets()


















