from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *
from outils import *
from back_game import Jeu
from back_players import Player
from front_map import Interface_Carte
from front_city import Interface_Ville
from front_unit import Interface_Unite
from front_player import Interface_Player

class Ecran_Accueil(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()
        self.configure(bg='black')

    def charger_images(self):
        """
        charge les différentes images nécesssaires à l'interface
        """
        self.images = {}
        tab = ["fond_accueil", "charger", "annule", "creer"]
        for elt in tab:
            self.images[str(elt)] = PhotoImage(file="../pics/window/ICON_" + str(elt) + ".png")

    def creer_widgets(self):
        self.btn_creer = Button(master = self)
        self.btn_annuler = Button(master=self)
        self.btn_charger = Button(master=self)
        self.lbl_fond = Label(master=self)

    def configurer_widgets(self):
        self.btn_creer.config( image=self.images["creer"],bg='black',
                  command=lambda: self.on_btn_new_click())
        self.btn_charger.config( image=self.images["charger"],bg='black',
                  command=lambda: self.on_btn_load_click())
        self.btn_annuler.config( image=self.images["annule"],bg='black',
                  command=lambda: self.on_btn_quit_click())
        self.lbl_fond.config(image = self.images["fond_accueil"],bg='black')

    def placer_widgets(self):
        self.btn_annuler.grid(row=1, column=0, columnspan=1)
        self.btn_creer.grid(row=1, column=2, columnspan=1)
        self.btn_charger.grid(row=1, column=1, columnspan=1)
        self.lbl_fond.grid(row=0, column=0, columnspan=3)

    def on_btn_new_click(self):
        formats_autorises = [("Cartes", "*.json")]
        map_adress = askopenfilename(title="choix du monde",
                                     initialdir="../maps",
                                     filetypes=formats_autorises)
        if map_adress != "":
            # fonction de chargement de la carte
            self.cacher()
            ecran = Ecran_Choix_Peuple(self.master, map_adress)
            ecran.grid(row=0, column=2, columnspan=2)

    def on_btn_load_click(self):
        print(">>> Sauvegardes non implémentées.")

    def on_btn_quit_click(self):
        reponse = askyesno(title="Vérification", message="Voulez-vous quitter ?")
        if reponse==True:
            self.master.destroy()

    def cacher(self):
        self.grid_remove()


##################################################################################
##################################################################################

class Ecran_Choix_Peuple(Frame):
    def __init__(self, master, map_adress:str):
        Frame.__init__(self, master)
        self.master = master
        self.adresse_carte = map_adress
        self.donnees_carte = charger_carte(map_adress)
        self.donnees_nations = charger_fichier_json("NATIONS")
        self.nations = []
        for nation_id in self.donnees_carte["playable_nation"]:
            self.nations.append(dict())
            self.nations[-1]["nation_id"] = nation_id
            self.nations[-1]["leader_file"] = self.donnees_nations[nation_id]["leader_picture"]
            self.nations[-1]["flag_file"] = self.donnees_nations[nation_id]["flag_picture"]
            self.nations[-1]["color"] = self.donnees_nations[nation_id]["color"]
            self.nations[-1]["name"] = self.donnees_nations[nation_id]["name"]
        self.indice = 0
        self.nation_selectionnee = self.nations[int(self.indice)]
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()
        self.configure(bg='black')

    def charger_images(self):
        """
        charge les différentes images nécesssaires à l'interface
        """
        self.images = {}
        tab2 = ["suivant", "precedent", 'confirmer', "annuler"]
        for elt in tab2:
            self.images[str(elt)] = PhotoImage(file="../pics/window/ICON_" + str(elt) + ".png")
        for nation in self.nations:
            leader = nation["leader_file"]
            flag = nation["flag_file"]
            self.images[nation["nation_id"]] = PhotoImage(file="../pics/window/" + str(leader))
            self.images["FLAG_"+nation["nation_id"]] = PhotoImage(file="../pics/nations/" + str(flag))

    def creer_widgets(self):
        self.btn_suivant = Button(master = self)
        self.btn_precedent = Button(master=self)
        self.btn_confirmer = Button(master=self)
        self.btn_annuler = Button(master=self)

        self.lbl_nation = Label(master=self)
        self.lbl_indicateur = Label(master=self)

    def configurer_widgets(self):
        self.btn_suivant.config(image=self.images["suivant"],
                                bg='black',
                                command=lambda: self.on_btn_suivant_click())
        self.btn_precedent.config(image=self.images["precedent"],
                                  bg='black',
                                  command=lambda: self.on_btn_precedent_click())
        self.btn_confirmer.config(image=self.images["confirmer"],
                                  bg='black',
                                  command=lambda: self.on_btn_confirmer_click())
        self.btn_annuler.config(image=self.images["annuler"],
                                bg='black',
                                command=lambda: self.on_btn_annuler_click())
        self.lbl_nation.config(image=self.images[self.nation_selectionnee["nation_id"]],
                               bg='black')
        self.lbl_indicateur.config(text=str(self.nation_selectionnee["name"]),
                                   image=self.images["FLAG_" + self.nation_selectionnee["nation_id"]],
                                   compound=BOTTOM,
                                   bg='black',fg=self.nation_selectionnee["color"],
                                   font=("Courier", 25))

    def placer_widgets(self):
        self.lbl_indicateur.grid(row=0, column=1, columnspan=2)
        self.btn_precedent.grid(row=1, column=0)
        self.lbl_nation.grid(row=1, column=1, columnspan=2)
        self.btn_suivant.grid(row=1, column=3)
        self.btn_annuler.grid(row=2, column=1)
        self.btn_confirmer.grid(row=2, column=2)


    def on_btn_suivant_click(self):
        self.indice += 1
        if self.indice >= len(self.nations):
            self.indice = 0
        self.nation_selectionnee = self.nations[int(self.indice)]
        self.configurer_widgets()

    def on_btn_precedent_click(self):
        self.indice -= 1
        if self.indice <= -1:
            self.indice = len(self.nations)-1
        self.nation_selectionnee = self.nations[int(self.indice)]
        self.configurer_widgets()

    def on_btn_confirmer_click(self):
        reponse_ok = askyesno(title="vérification", message="Voulez-vous choisir cette nation ?")
        if reponse_ok:
            self.grid_remove()
            jeu = Jeu(self.adresse_carte, self.nation_selectionnee["nation_id"])
            ecran = Ecran_Jeu(self.master, jeu)
            ecran.grid()
            #lancer_le_jeu(self.nations[self.indice])

    def on_btn_annuler_click(self):
        reponse_annuler = askyesno(title="vérification", message="Voulez-vous revenir à l'accueil ?")
        if reponse_annuler:
            self.grid_remove()
            ecran=Ecran_Accueil(self.master)
            ecran.grid(row=0, column=2, columnspan=2)

    def cacher(self):
        self.destroy()

##################################################################################
##################################################################################


class Ecran_Jeu(Frame):
    def __init__(self, master, game: Jeu):
        Frame.__init__(self, master)
        self.master = master
        self.game = game
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()

    def charger_images(self):
        """
        charge les différentes images nécesssaires à l'interface
        """
        self.images = {}
        self.images["annule"] = PhotoImage(file="../pics/window/ICON_annule.png")
        self.images["accueil"] = PhotoImage(file="../pics/window/ICON_home.png")
        self.images["tour_suivant"] = PhotoImage(file="../pics/window/ICON_nexturn.png")

    def creer_widgets(self):
        self.interface_carte = Interface_Carte(self, self.game)
        self.interface_unite = Interface_Unite(self)
        self.interface_ville = Interface_Ville(self,self.game)
        self.interface_joueur = Interface_Player(self, self.game.joueur_actif)
        self.btn_quit = Button(self,
                               image=self.images["annule"],bg='black',
                               command=self.on_btn_quit_click)
        self.btn_home = Button(self,
                               image=self.images["accueil"],bg='black',
                               command=self.on_btn_home_click)
        self.btn_end_of_turn = Button(self,
                                      image=self.images["tour_suivant"],bg='black',
                                      command=self.on_btn_end_of_turn_click)

    def configurer_widgets(self):
        self.master.bind_all("<<INFO-UPDATE>>", self.actualiser)

    def placer_widgets(self):
        self.interface_carte.grid(row=0, column=0, columnspan=3, rowspan=4)
        self.interface_unite.grid(row=4, column=0)
        self.interface_ville.grid(row=4, column=1)
        self.interface_joueur.grid(row=4, column=2)

        self.btn_quit.grid(row=0, column=3)
        self.btn_home.grid(row=1, column=3)
        self.btn_end_of_turn.grid(row=2, column=3)

    def on_btn_quit_click(self):
        self.master.destroy()

    def on_btn_home_click(self):
        pass

    def actualiser(self, evt:Event):
        self.interface_unite.actualiser()
        if self.game.ville_selectionnee is not None:
            self.interface_ville.actualiser(self.game.ville_selectionnee)
        self.interface_joueur.actualiser()

    def on_btn_end_of_turn_click(self):
        self.game.joueur_suivant()
        self.actualiser(None)

if __name__ == "__main__":
    fenetre = Tk()
    """ecran = Ecran_Accueil(fenetre)
    ecran.grid(row=0, column=2, columnspan=2)"""
    j = Jeu("../maps/MAP_WORLD.json", "NATION_France")
    ecran = Ecran_Jeu(fenetre,j)
    ecran.grid()
    fenetre.mainloop()
